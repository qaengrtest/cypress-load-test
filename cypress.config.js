const { lighthouse, prepareAudit } = require('@cypress-audit/lighthouse');
const { pa11y } = require('@cypress-audit/pa11y');
const { defineConfig } = require('cypress');

module.exports = defineConfig({
    e2e: {
        baseUrl: 'https://test.legalmatch.com/', // this is your app
        setupNodeEvents(on, config) {
            on('before:browser:launch', (browser = {}, launchOptions) => {
                prepareAudit(launchOptions);
            });

            on('task', {
                lighthouse: lighthouse(),
                pa11y: pa11y((pa11yReport) => {
                    console.log(pa11yReport); // raw pa11y reports
                })
            });
        }
    }
});
